from django.shortcuts import render
from user.models import User

# Create your views here.
def index(request):
	user = User.objects.all().first()
	context = {
		'user': user
	}

	return render(request, 'user/index.html', context)