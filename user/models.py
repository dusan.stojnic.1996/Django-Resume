from django.db import models

# Create your models here.
class User(models.Model):
	firstName = models.CharField(max_length=40)
	lastName = models.CharField(max_length=40)
	description = models.TextField()
	city = models.CharField(max_length=60)
	country = models.CharField(max_length=60)
	email = models.CharField(max_length=200);
	#tags = TaggableManager()